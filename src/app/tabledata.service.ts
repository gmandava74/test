import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TabledataService {

  constructor(private httpService: HttpClient) { }
  tabledata() {
    return this.httpService.get("../../assets/sample_data.json")
  }
}
