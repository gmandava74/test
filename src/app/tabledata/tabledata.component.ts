import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-tabledata',
  templateUrl: './tabledata.component.html',
  styleUrls: ['./tabledata.component.scss']
})
export class TabledataComponent implements OnInit {

  length: any;
  startIndex = 0;
  getendindex = 5;
  title = 'customer-app';
  sample_datafile: string[];
  constructor(private httpService: HttpClient) { }
  getarrayfromnumber(length) {
    return new Array(length);
  }
  updateIndex(pageIndex) {
    this.startIndex = pageIndex * 5;
    this.getendindex = this.startIndex + 5;
  }

  ngOnInit() {
    this.httpService.get('./../assets/sample_data.json').subscribe(
      data => {
        this.sample_datafile = data as string[];
        console.log(this.sample_datafile);
      }
    );
  }

}
